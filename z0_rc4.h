#ifndef Z0_RC4_H
#define Z0_RC4_H

#include <stdint.h>

void                                        
z0_rc4(const uint8_t*, size_t, const uint8_t*, uint8_t, uint8_t*);                       

#endif
